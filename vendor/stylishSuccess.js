/* eslint-disable @typescript-eslint/no-var-requires */
"use strict";

const stylish = require("eslint/lib/cli-engine/formatters/stylish");
const chalk = require("chalk");

/**
 * Provides an implementation of Stylish that has an output on success as well as failure. Perfect for watch mode.
 *
 * @author Cam Morrow
 *
 * @param {Array}   results
 * @returns {String}
 */
module.exports = (results) => {
    const styled = stylish(results);
    const succeeded = styled.length === 0;
    const timestampMessage = `Evaluated at ${new Date()}`;
    const chalker = succeeded ? chalk.green.bold : chalk.red.bold;

    if (succeeded) {
        return `${chalker(timestampMessage)}\n${chalker("✓️ No problems found.")}`;
    } else {
        return `${styled}\n${chalker(timestampMessage)}`;
    }
};
