import { IProgressTracker, IProgressTrackerStatePayload, ProgressTrackerStateType } from "..";

type AnyProgressTracker = IProgressTracker<ProgressTrackerStateType, IProgressTrackerStatePayload<ProgressTrackerStateType>>;

const progressTrackerParentageFactory = () => {
    const parentage = new WeakMap<AnyProgressTracker, Set<AnyProgressTracker>>();

    const register = (child: AnyProgressTracker, parent: AnyProgressTracker) => {
        const parentSet = parentage.get(child) || new Set<AnyProgressTracker>();
        parentSet.add(parent);
        parentage.set(child, parentSet);
    };

    const deregister = (child: AnyProgressTracker, parent: AnyProgressTracker) => {
        const parentSet = parentage.get(child);
        if (parentSet) {
            parentSet.delete(parent);
            if (parentSet.size === 0) {
                parentage.delete(child);
            }
        }
    };

    const hasParent = (child: AnyProgressTracker, parent: AnyProgressTracker): boolean => {
        const parentSet = parentage.get(child);
        if (parentSet) {
            return parentSet.has(parent);
        }

        return false;
    };

    const hasAncestor = (child: AnyProgressTracker, parent: AnyProgressTracker): boolean => {
        const parentSet = parentage.get(child);
        if (parentSet) {
            // Direct descendant?
            if (parentSet.has(parent)) {
                return true;
            }

            // See if any of it's parents are children of the candidate
            for (const midParent of parentSet.values()) {
                if (hasAncestor(midParent, parent)) {
                    return true;
                }
            }
        }

        return false;
    };

    return {
        register,
        deregister,
        hasParent,
        hasAncestor,
    };
};

export const progressTrackerParentage = progressTrackerParentageFactory();
