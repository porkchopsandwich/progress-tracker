import { eventAutobus, EventAutobusListener, IEventAutobusSubscribeResult } from "@porkchopsandwich/event-autobus";
import { ChildProgressTracker, IParentProgressTracker } from "./interfaces/IParentProgressTracker";
import { ChildProgressTrackerStatePayload, IParentProgressTrackerStatePayload } from "./interfaces/IParentProgressTrackerStatePayload";
import { ProgressTrackerStateMap } from "./interfaces/ProgressTrackerStateMap";
import { ProgressTrackerStateType } from "./interfaces/ProgressTrackerStateType";
import { progressTrackerParentage } from "./utils/progressTrackerParentage";

interface IParentProgressTrackerBusTypes<TStates extends ProgressTrackerStateType> {
    progress: IParentProgressTrackerStatePayload<TStates>;
}

export const parentProgressTracker = <TStates extends ProgressTrackerStateType>(): IParentProgressTracker<TStates> => {
    const childProgressTrackers = new Set<ChildProgressTracker<TStates>>();
    const bus = eventAutobus<IParentProgressTrackerBusTypes<TStates>>();
    const subscriptions = new WeakMap<ChildProgressTracker<TStates>, IEventAutobusSubscribeResult>();
    // eslint-disable-next-line prefer-const
    let instance: IParentProgressTracker<TStates>;

    const getCurrentState = (): IParentProgressTrackerStatePayload<TStates> => {
        const childStates = new WeakMap<ChildProgressTracker<TStates>, ChildProgressTrackerStatePayload<TStates>>();
        let progressedStates = 0;
        let maxStates = 0;
        const stateMap: ProgressTrackerStateMap<TStates> = {};
        const stateList: (TStates | undefined)[] = [];

        childProgressTrackers.forEach((childProgressTracker) => {
            const childState = childProgressTracker.getCurrentState();
            const childStateMap = childState.stateMap;
            progressedStates += childState.progressedStates;
            maxStates += childState.maxStates;
            for (const state in childStateMap) {
                if (childStateMap.hasOwnProperty(state)) {
                    const currentStateMapState = (stateMap[state] as number) || 0;
                    const childStateMapState = childStateMap[state] as number;
                    stateMap[state] = currentStateMapState + childStateMapState;
                }
            }
            stateList.push(...childState.stateList);
            childStates.set(childProgressTracker, childState);
        });

        return {
            childStates,
            stateMap,
            stateList,
            progressedStates,
            maxStates,
        };
    };

    const publish = () => {
        bus.publish("progress", getCurrentState());
    };

    const add = (...progressTrackers: ChildProgressTracker<TStates>[]) => {
        for (const progressTracker of progressTrackers) {
            if (progressTracker === instance) {
                throw new Error(`A Progress Tracker cannot be added as a child of itself.`);
            } else if (progressTrackerParentage.hasAncestor(instance, progressTracker)) {
                throw new Error(`A Progress Tracker cannot be added as a child when it is already a ancestor.`);
            } else {
                progressTrackerParentage.register(progressTracker, instance);
                childProgressTrackers.add(progressTracker);
                subscriptions.set(progressTracker, progressTracker.subscribe(publish));
            }
        }
        publish();
    };

    const remove = (...progressTrackers: ChildProgressTracker<TStates>[]) => {
        for (const progressTracker of progressTrackers) {
            childProgressTrackers.delete(progressTracker);
            const subscription = subscriptions.get(progressTracker);
            if (subscription) {
                subscription.unsubscribe();
            }
        }
        publish();
    };

    const clear = () => {
        childProgressTrackers.clear();
        publish();
    };

    const subscribe = (listener: EventAutobusListener<IParentProgressTrackerStatePayload<TStates>>) => {
        return bus.subscribe("progress", listener);
    };

    instance = {
        add,
        remove,
        clear,
        subscribe,
        getCurrentState,
    };

    return instance;
};
