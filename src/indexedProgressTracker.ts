import { eventAutobus, EventAutobusListener } from "@porkchopsandwich/event-autobus";
import { IIndexedProgressTracker } from "./interfaces/IIndexedProgressTracker";
import { IProgressTrackerStatePayload } from "./interfaces/IProgressTrackerStatePayload";
import { ProgressTrackerStateMap } from "./interfaces/ProgressTrackerStateMap";
import { ProgressTrackerStateType } from "./interfaces/ProgressTrackerStateType";

interface IIndexedProgressTrackerBusTypes<TStates extends ProgressTrackerStateType> {
    progress: IProgressTrackerStatePayload<TStates>;
}

export const indexedProgressTracker = <TStates extends ProgressTrackerStateType>(initialMaxStates: number): IIndexedProgressTracker<TStates> => {
    const bus = eventAutobus<IIndexedProgressTrackerBusTypes<TStates>>();
    let stateList: (TStates | undefined)[];
    let maxStates = initialMaxStates;

    const getCurrentState = (): IProgressTrackerStatePayload<TStates> => {
        const stateMap: ProgressTrackerStateMap<TStates> = {};

        const progressedStates = stateList.reduce((rollingTotal, state) => {
            if (state !== undefined) {
                const currentStateCount = (stateMap[state] as number) || 0;
                stateMap[state] = currentStateCount + 1;
            }

            return rollingTotal + (state === undefined ? 0 : 1);
        }, 0);

        return {
            maxStates,
            progressedStates,
            stateList,
            stateMap,
        };
    };

    const publish = () => {
        bus.publish("progress", getCurrentState());
    };

    const reset = (newMaxStates: number) => {
        maxStates = newMaxStates;
        stateList = [];
        for (let i = 0; i < maxStates; i++) {
            stateList[i] = undefined;
        }
        publish();
    };

    const subscribe = (listener: EventAutobusListener<IProgressTrackerStatePayload<TStates>>) => {
        return bus.subscribe("progress", listener);
    };

    const progress = (index: number, state: TStates) => {
        if (index < 0 || index >= maxStates) {
            throw new RangeError(`Progress Tracker progress index is outside the valid boundaries of 0 to ${maxStates}.`);
        }

        stateList[index] = state;
        publish();
    };

    reset(initialMaxStates);

    return {
        subscribe,
        progress,
        maxStates,
        reset,
        getCurrentState,
    };
};
