import { eventAutobus, EventAutobusListener } from "@porkchopsandwich/event-autobus";
import { IProgressTrackerStatePayload } from "./interfaces/IProgressTrackerStatePayload";
import { ISequentialProgressTracker } from "./interfaces/ISequentialProgressTracker";
import { ProgressTrackerStateMap } from "./interfaces/ProgressTrackerStateMap";
import { ProgressTrackerStateType } from "./interfaces/ProgressTrackerStateType";

interface ISequentialProgressTrackerBusTypes<TStates extends ProgressTrackerStateType> {
    progress: IProgressTrackerStatePayload<TStates>;
}

export const sequentialProgressTracker = <TStates extends ProgressTrackerStateType>(initialMaxStates: number): ISequentialProgressTracker<TStates> => {
    const bus = eventAutobus<ISequentialProgressTrackerBusTypes<TStates>>();
    let progressedStates: number;
    let maxStates = initialMaxStates;
    let stateMap: ProgressTrackerStateMap<TStates>;
    let stateList: (TStates | undefined)[];

    const getCurrentState = (): IProgressTrackerStatePayload<TStates> => {
        return {
            maxStates,
            progressedStates,
            stateMap,
            stateList,
        };
    };

    const publish = () => {
        bus.publish("progress", getCurrentState());
    };

    const reset = (newMaxStates: number) => {
        maxStates = newMaxStates;
        progressedStates = 0;
        stateMap = {};
        stateList = [];
        publish();
    };

    const subscribe = (listener: EventAutobusListener<IProgressTrackerStatePayload<TStates>>) => {
        return bus.subscribe("progress", listener);
    };

    const progressState = (state: TStates) => {
        if (progressedStates === maxStates) {
            throw new RangeError(`Progress Tracker has already received the maximum number of ${maxStates} progressions.`);
        }
        progressedStates++;
        stateList.push(state);
        stateMap[state] = stateMap[state] === undefined ? 1 : (stateMap[state] as number) + 1;
        publish();
    };

    const progress = (...states: TStates[]) => {
        states.forEach(progressState);
    };

    reset(initialMaxStates);

    return {
        subscribe,
        progress,
        maxStates,
        reset,
        getCurrentState,
    };
};
