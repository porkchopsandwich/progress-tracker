import { ChildProgressTracker, ChildProgressTrackerStatePayload, IParentProgressTrackerStatePayload, indexedProgressTracker, parentProgressTracker, sequentialProgressTracker, IProgressTrackerStatePayload } from "..";
import { State, stateLooksLike } from "./utils";

const blankState: Partial<IParentProgressTrackerStatePayload<State>> = {
    maxStates: 0,
    progressedStates: 0,
    stateMap: {},
};

const parentStateChildStateLooksLike = (state: IParentProgressTrackerStatePayload<State>, childProgressTracker: ChildProgressTracker<State>, object: Partial<ChildProgressTrackerStatePayload<State>>): void => {
    const childProgressTrackerStateInParentState = state.childStates.get(childProgressTracker);

    expect(childProgressTrackerStateInParentState).toBeTruthy();

    expect(childProgressTracker.getCurrentState()).toEqual(childProgressTrackerStateInParentState);
    if (childProgressTrackerStateInParentState) {
        stateLooksLike(childProgressTrackerStateInParentState, object);
    }
};

test("Can handle simple recursive relationships.", () => {
    const firstParentProgressTracker = parentProgressTracker<State>();
    const secondParentProgressTracker = parentProgressTracker<State>();
    const childProgressTracker = sequentialProgressTracker<State>(3);

    // Make the second a child of the first
    firstParentProgressTracker.add(secondParentProgressTracker);

    // Make the first a child of the second - this should be an error
    expect(() => {
        secondParentProgressTracker.add(firstParentProgressTracker);
    }).toThrowError(Error);

    // Make the sequential a child of the second
    secondParentProgressTracker.add(childProgressTracker);

    // Trigger progress
    childProgressTracker.progress("success");

    const currentState = firstParentProgressTracker.getCurrentState();
    const expectedState: Partial<IProgressTrackerStatePayload<State>> = {
        maxStates: 3,
        progressedStates: 1,
        stateMap: {
            success: 1,
        },
        stateList: ["success"],
    };
    stateLooksLike(currentState, expectedState);
    parentStateChildStateLooksLike(currentState, secondParentProgressTracker, expectedState);
    stateLooksLike(childProgressTracker.getCurrentState(), expectedState);
});

test("Can handle complex recursive relationships.", () => {
    const firstParentProgressTracker = parentProgressTracker<State>();
    const secondParentProgressTracker = parentProgressTracker<State>();
    const thirdParentProgressTracker = parentProgressTracker<State>();
    const childProgressTracker = sequentialProgressTracker<State>(3);

    // Make the second a child of the first
    firstParentProgressTracker.add(secondParentProgressTracker);

    // Make the third a child of the second
    secondParentProgressTracker.add(thirdParentProgressTracker);

    // Make the first a child of the third
    expect(() => {
        thirdParentProgressTracker.add(firstParentProgressTracker);
    }).toThrowError(Error);

    // Make the sequential a child of the third
    thirdParentProgressTracker.add(childProgressTracker);

    // Trigger and see what happens
    childProgressTracker.progress("success");

    const expectedState: Partial<IProgressTrackerStatePayload<State>> = {
        maxStates: 3,
        progressedStates: 1,
        stateMap: {
            success: 1,
        },
        stateList: ["success"],
    };
    const firstCurrentState = firstParentProgressTracker.getCurrentState();
    const secondCurrentState = secondParentProgressTracker.getCurrentState();
    stateLooksLike(firstCurrentState, expectedState);
    parentStateChildStateLooksLike(firstCurrentState, secondParentProgressTracker, expectedState);
    stateLooksLike(secondCurrentState, expectedState);
    parentStateChildStateLooksLike(secondCurrentState, thirdParentProgressTracker, expectedState);
});

test("Can clear listeners.", () => {
    const progressTracker = parentProgressTracker<State>();

    const listener = jest.fn();

    const subscription = progressTracker.subscribe(listener);

    expect(listener).toBeCalledTimes(0);

    progressTracker.clear();

    expect(listener).toBeCalledTimes(1);

    subscription.unsubscribe();

    progressTracker.clear();

    expect(listener).toBeCalledTimes(1);
});

test("Can add child progress trackers.", () => {
    const progressTracker = parentProgressTracker<State>();

    const listener = jest.fn();
    progressTracker.subscribe(listener);

    const childProgressTracker1 = indexedProgressTracker<State>(1);
    progressTracker.add(childProgressTracker1);

    // Called immediately after adding child
    expect(listener).toBeCalledTimes(1);

    childProgressTracker1.progress(0, "success");

    // Called after child updates
    expect(listener).toBeCalledTimes(2);
});

test("Can remove child progress trackers.", () => {
    const progressTracker = parentProgressTracker<State>();

    const listener = jest.fn();
    progressTracker.subscribe(listener);

    const childProgressTracker1 = indexedProgressTracker<State>(1);
    progressTracker.add(childProgressTracker1);

    // Called immediately after adding child, and again after progress
    childProgressTracker1.progress(0, "success");
    expect(listener).toBeCalledTimes(2);

    progressTracker.remove(childProgressTracker1);

    // Called after removing child
    expect(listener).toBeCalledTimes(3);

    childProgressTracker1.progress(0, "failure");

    // Not called again, because the child has been removed
    expect(listener).toBeCalledTimes(3);

    // Add it back
    progressTracker.add(childProgressTracker1);

    // Updated from the add
    expect(listener).toBeCalledTimes(4);

    // Remove all children
    progressTracker.clear();

    // One more update from the clear
    expect(listener).toBeCalledTimes(5);
});

test("Can get state updated from children.", () => {
    const progressTracker = parentProgressTracker<State>();

    stateLooksLike(progressTracker.getCurrentState(), blankState);

    const childProgressTracker1 = indexedProgressTracker<State>(2);
    const childProgressTracker2 = sequentialProgressTracker<State>(2);
    progressTracker.add(childProgressTracker1, childProgressTracker2);

    let currentState = progressTracker.getCurrentState();
    stateLooksLike(currentState, {
        maxStates: 4,
        progressedStates: 0,
        stateMap: {},
    });
    parentStateChildStateLooksLike(currentState, childProgressTracker1, {
        stateList: [undefined, undefined],
        maxStates: 2,
        progressedStates: 0,
    });
    parentStateChildStateLooksLike(currentState, childProgressTracker2, {
        stateMap: {},
        maxStates: 2,
        progressedStates: 0,
    });

    childProgressTracker1.progress(0, "success");

    currentState = progressTracker.getCurrentState();
    stateLooksLike(progressTracker.getCurrentState(), {
        maxStates: 4,
        progressedStates: 1,
        stateMap: {
            success: 1,
        },
    });
    parentStateChildStateLooksLike(currentState, childProgressTracker1, {
        stateList: ["success", undefined],
        maxStates: 2,
        progressedStates: 1,
    });
    parentStateChildStateLooksLike(currentState, childProgressTracker2, {
        stateMap: {},
        maxStates: 2,
        progressedStates: 0,
    });

    childProgressTracker2.progress("failure");

    currentState = progressTracker.getCurrentState();
    stateLooksLike(progressTracker.getCurrentState(), {
        maxStates: 4,
        progressedStates: 2,
        stateMap: {
            success: 1,
            failure: 1,
        },
    });
    parentStateChildStateLooksLike(currentState, childProgressTracker1, {
        stateList: ["success", undefined],
        maxStates: 2,
        progressedStates: 1,
    });
    parentStateChildStateLooksLike(currentState, childProgressTracker2, {
        stateMap: {
            failure: 1,
        },
        maxStates: 2,
        progressedStates: 1,
    });

    childProgressTracker1.progress(0, "failure");
    childProgressTracker1.progress(1, "failure");

    currentState = progressTracker.getCurrentState();
    stateLooksLike(progressTracker.getCurrentState(), {
        maxStates: 4,
        progressedStates: 3,
        stateMap: {
            failure: 3,
        },
    });
    parentStateChildStateLooksLike(currentState, childProgressTracker1, {
        stateList: ["failure", "failure"],
        maxStates: 2,
        progressedStates: 2,
    });
    parentStateChildStateLooksLike(currentState, childProgressTracker2, {
        stateMap: {
            failure: 1,
        },
        maxStates: 2,
        progressedStates: 1,
    });
});
