import { IProgressTrackerStatePayload } from "..";

export type State = "success" | "failure";

export const stateLooksLike = (state: IProgressTrackerStatePayload<State>, candidate: Partial<IProgressTrackerStatePayload<State>>): void => {
    expect(state).toEqual(expect.objectContaining(candidate));
};
