import { IProgressTrackerStatePayload, sequentialProgressTracker } from "..";
import { State, stateLooksLike } from "./utils";

test("Can instantiate and get an initial state.", () => {
    const progressTracker = sequentialProgressTracker<State>(0);
    stateLooksLike(progressTracker.getCurrentState(), {
        maxStates: 0,
        progressedStates: 0,
        stateMap: {},
        stateList: [],
    });
});

test("Can listen to progress.", () => {
    const maxStates = 3;
    let progressedStates = 0;
    const progressTracker = sequentialProgressTracker<State>(maxStates);

    const states: Partial<IProgressTrackerStatePayload<State>>[] = [
        {
            maxStates,
            progressedStates: 1,
            stateMap: {
                success: 1,
            },
            stateList: ["success"],
        },
        {
            maxStates,
            progressedStates: 2,
            stateMap: {
                success: 2,
            },
            stateList: ["success", "success"],
        },
    ];

    const listener = jest.fn((state: IProgressTrackerStatePayload<State>) => {
        stateLooksLike(state, states[progressedStates]);
        progressedStates++;
    });
    const anotherListener = jest.fn();

    const subscription = progressTracker.subscribe(listener);
    progressTracker.subscribe(anotherListener);

    expect(listener).toBeCalledTimes(0);
    expect(anotherListener).toBeCalledTimes(0);

    progressTracker.progress("success");

    expect(listener).toBeCalledTimes(1);
    expect(anotherListener).toBeCalledTimes(1);

    progressTracker.progress("success");

    subscription.unsubscribe();

    progressTracker.progress("success");

    expect(listener).toBeCalledTimes(2);
    expect(anotherListener).toBeCalledTimes(3);
});

test("Can progress and throw errors.", () => {
    const maxStates = 2;
    let progressedStates = 0;
    const progressTracker = sequentialProgressTracker<State>(maxStates);

    const states: Partial<IProgressTrackerStatePayload<State>>[] = [
        {
            maxStates,
            progressedStates: 1,
            stateMap: {
                success: 1,
            },
            stateList: ["success"],
        },
        {
            maxStates,
            progressedStates: 2,
            stateMap: {
                success: 1,
                failure: 1,
            },
            stateList: ["success", "failure"],
        },
    ];

    const listener = jest.fn((state: IProgressTrackerStatePayload<State>) => {
        stateLooksLike(state, states[progressedStates]);
        progressedStates++;
    });

    progressTracker.subscribe(listener);

    stateLooksLike(progressTracker.getCurrentState(), {
        maxStates,
        progressedStates: 0,
        stateMap: {},
        stateList: [],
    });

    expect(listener).toBeCalledTimes(0);

    progressTracker.progress("success");

    expect(listener).toBeCalledTimes(1);

    progressTracker.progress("failure");

    expect(listener).toBeCalledTimes(2);

    expect(() => {
        progressTracker.progress("success");
    }).toThrowError(RangeError);

    stateLooksLike(progressTracker.getCurrentState(), states[1]);
});

test("Can reset", () => {
    const maxStates = 2;
    const nextMaxStates = 1;
    let progressedStates = 0;
    const progressTracker = sequentialProgressTracker<State>(maxStates);

    const states: Partial<IProgressTrackerStatePayload<State>>[] = [
        {
            maxStates,
            progressedStates: 1,
            stateMap: {
                success: 1,
            },
            stateList: ["success"],
        },
        {
            maxStates,
            progressedStates: 2,
            stateMap: {
                success: 1,
                failure: 1,
            },
            stateList: ["success", "failure"],
        },
        {
            maxStates: nextMaxStates,
            progressedStates: 0,
            stateMap: {},
            stateList: [],
        },
        {
            maxStates: nextMaxStates,
            progressedStates: 1,
            stateMap: {
                failure: 1,
            },
            stateList: ["failure"],
        },
    ];

    const listener = jest.fn((state: IProgressTrackerStatePayload<State>) => {
        stateLooksLike(state, states[progressedStates]);
        progressedStates++;
    });

    progressTracker.subscribe(listener);

    progressTracker.progress("success");
    progressTracker.progress("failure");

    expect(listener).toBeCalledTimes(2);

    progressTracker.reset(nextMaxStates);

    progressTracker.progress("failure");

    expect(listener).toBeCalledTimes(4);
});
