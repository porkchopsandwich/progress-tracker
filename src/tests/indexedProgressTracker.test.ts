import { IProgressTrackerStatePayload, indexedProgressTracker } from "..";
import { stateLooksLike, State } from "./utils";

const blankState: IProgressTrackerStatePayload<State> = {
    maxStates: 0,
    progressedStates: 0,
    stateList: [],
    stateMap: {},
};

test("Can instantiate and get an initial state.", () => {
    const progressTracker = indexedProgressTracker<State>(0);
    stateLooksLike(progressTracker.getCurrentState(), blankState);
});

test("Can listen to progress.", () => {
    const maxStates = 2;
    let listenerCounter = 0;
    const progressTracker = indexedProgressTracker<State>(maxStates);

    const states: IProgressTrackerStatePayload<State>[] = [
        {
            maxStates,
            progressedStates: 1,
            stateList: ["success", undefined],
            stateMap: {
                success: 1,
            },
        },
    ];

    stateLooksLike(progressTracker.getCurrentState(), { maxStates, progressedStates: 0, stateList: [undefined, undefined] });

    const listener = jest.fn((state: IProgressTrackerStatePayload<State>) => {
        stateLooksLike(state, states[listenerCounter]);
        listenerCounter++;
    });
    const anotherListener = jest.fn();

    const subscription = progressTracker.subscribe(listener);
    progressTracker.subscribe(anotherListener);

    expect(listener).toBeCalledTimes(0);
    expect(anotherListener).toBeCalledTimes(0);

    progressTracker.progress(0, "success");

    expect(listener).toBeCalledTimes(1);
    expect(anotherListener).toBeCalledTimes(1);

    subscription.unsubscribe();

    progressTracker.progress(0, "success");

    expect(listener).toBeCalledTimes(1);
    expect(anotherListener).toBeCalledTimes(2);
});

test("Can progress and throw errors.", () => {
    const maxStates = 2;
    let listenerCounter = 0;
    const progressTracker = indexedProgressTracker<State>(maxStates);

    const states: IProgressTrackerStatePayload<State>[] = [
        {
            maxStates,
            progressedStates: 1,
            stateList: ["success", undefined],
            stateMap: {
                success: 1,
            },
        },
        {
            maxStates,
            progressedStates: 2,
            stateList: ["success", "failure"],
            stateMap: {
                success: 1,
                failure: 1,
            },
        },
    ];

    const listener = jest.fn((state: IProgressTrackerStatePayload<State>) => {
        stateLooksLike(state, states[listenerCounter]);
        listenerCounter++;
    });

    progressTracker.subscribe(listener);

    stateLooksLike(progressTracker.getCurrentState(), { maxStates, progressedStates: 0, stateList: [undefined, undefined], stateMap: {} });

    expect(listener).toBeCalledTimes(0);

    progressTracker.progress(0, "success");

    expect(listener).toBeCalledTimes(1);

    progressTracker.progress(1, "failure");

    expect(listener).toBeCalledTimes(2);

    expect(() => {
        progressTracker.progress(2, "success");
    }).toThrowError(RangeError);

    stateLooksLike(progressTracker.getCurrentState(), states[1]);
});

test("Can reset", () => {
    const maxStates = 2;

    const newMaxStates = 1;
    let listenerCounter = 0;
    const progressTracker = indexedProgressTracker<State>(maxStates);

    const states: IProgressTrackerStatePayload<State>[] = [
        {
            maxStates,
            progressedStates: 1,
            stateList: [undefined, "success"],
            stateMap: {
                success: 1,
            },
        },
        {
            maxStates,
            progressedStates: 2,
            stateList: ["failure", "success"],
            stateMap: {
                success: 1,
                failure: 1,
            },
        },
        {
            maxStates: newMaxStates,
            progressedStates: 0,
            stateList: [undefined],
            stateMap: {},
        },
        {
            maxStates: newMaxStates,
            progressedStates: 1,
            stateList: ["failure"],
            stateMap: {
                failure: 1,
            },
        },
        {
            maxStates: newMaxStates,
            progressedStates: 0,
            stateList: [undefined],
            stateMap: {},
        },
    ];

    const listener = jest.fn((state: IProgressTrackerStatePayload<State>) => {
        stateLooksLike(state, states[listenerCounter]);
        listenerCounter++;
    });

    progressTracker.subscribe(listener);

    progressTracker.progress(1, "success");
    progressTracker.progress(0, "failure");

    expect(listener).toBeCalledTimes(2);
    progressTracker.reset(newMaxStates);

    progressTracker.progress(0, "failure");

    expect(listener).toBeCalledTimes(4);

    progressTracker.reset(newMaxStates);
    stateLooksLike(progressTracker.getCurrentState(), states[4]);
});
