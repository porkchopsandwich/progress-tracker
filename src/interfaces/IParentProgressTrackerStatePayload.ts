import { ChildProgressTracker } from "./IParentProgressTracker";
import { IProgressTrackerStatePayload } from "./IProgressTrackerStatePayload";
import { ProgressTrackerStateType } from "./ProgressTrackerStateType";

export type ChildProgressTrackerStatePayload<TStates extends ProgressTrackerStateType> = IProgressTrackerStatePayload<TStates> | IParentProgressTrackerStatePayload<TStates>;

export interface IParentProgressTrackerStatePayload<TStates extends ProgressTrackerStateType> extends IProgressTrackerStatePayload<TStates> {
    childStates: Readonly<WeakMap<ChildProgressTracker<TStates>, ChildProgressTrackerStatePayload<TStates>>>;
}
