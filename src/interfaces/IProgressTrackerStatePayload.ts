import { ProgressTrackerStateMap } from "./ProgressTrackerStateMap";
import { ProgressTrackerStateType } from "./ProgressTrackerStateType";

export interface IProgressTrackerStatePayload<TStates extends ProgressTrackerStateType> {
    maxStates: Readonly<number>;
    progressedStates: Readonly<number>;
    stateMap: Readonly<ProgressTrackerStateMap<TStates>>;
    stateList: Readonly<(TStates | undefined)[]>;
}
