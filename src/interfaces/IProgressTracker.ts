import { IProgressTrackerStatePayload } from "./IProgressTrackerStatePayload";
import { ProgressTrackerStateType } from "./ProgressTrackerStateType";
import { IEventAutobusSubscribeResult, EventAutobusListener } from "@porkchopsandwich/event-autobus";

export interface IProgressTracker<TStates extends ProgressTrackerStateType, TStatePayload extends IProgressTrackerStatePayload<TStates>> {
    subscribe(listener: EventAutobusListener<TStatePayload>): IEventAutobusSubscribeResult;
    getCurrentState(): TStatePayload;
}
