import { IProgressTracker } from "./IProgressTracker";
import { IProgressTrackerStatePayload } from "./IProgressTrackerStatePayload";
import { ProgressTrackerStateType } from "./ProgressTrackerStateType";

export interface IIndexedProgressTracker<TStates extends ProgressTrackerStateType> extends IProgressTracker<TStates, IProgressTrackerStatePayload<TStates>> {
    progress(index: number, state: TStates): void;
    reset(maxStates: number): void;
    maxStates: Readonly<number>;
}
