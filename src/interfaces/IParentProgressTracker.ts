import { IIndexedProgressTracker } from "./IIndexedProgressTracker";
import { IParentProgressTrackerStatePayload } from "./IParentProgressTrackerStatePayload";
import { IProgressTracker } from "./IProgressTracker";
import { ISequentialProgressTracker } from "./ISequentialProgressTracker";
import { ProgressTrackerStateType } from "./ProgressTrackerStateType";

export type ChildProgressTracker<TStates extends ProgressTrackerStateType> = IIndexedProgressTracker<TStates> | ISequentialProgressTracker<TStates> | IParentProgressTracker<TStates>;

export interface IParentProgressTracker<TStates extends ProgressTrackerStateType> extends IProgressTracker<TStates, IParentProgressTrackerStatePayload<TStates>> {
    add(...progressTrackers: ChildProgressTracker<TStates>[]): void;
    remove(...progressTrackers: ChildProgressTracker<TStates>[]): void;
    clear(): void;
}
