import { IProgressTracker } from "./IProgressTracker";
import { IProgressTrackerStatePayload } from "./IProgressTrackerStatePayload";
import { ProgressTrackerStateType } from "./ProgressTrackerStateType";

export interface ISequentialProgressTracker<TStates extends ProgressTrackerStateType> extends IProgressTracker<TStates, IProgressTrackerStatePayload<TStates>> {
    progress(...states: TStates[]): void;
    reset(maxStates: number): void;
    maxStates: Readonly<number>;
}
