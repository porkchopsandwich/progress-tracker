import { ProgressTrackerStateType } from "./ProgressTrackerStateType";

type ProgressTrackerFullStateMap<TStates extends ProgressTrackerStateType> = { [P in TStates]: number };

// export type ProgressTrackerStateMap<TStates extends ProgressTrackerStateType> = { [P in TStates]?: number };
export type ProgressTrackerStateMap<TStates extends ProgressTrackerStateType> = Partial<ProgressTrackerFullStateMap<TStates>>;
