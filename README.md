# Progress Tracker

Sequential or parallel progress tracking, and nested progress trackers.

Requires support (native or polyfill) for Map/Set/WeakMap.

## Installation

```
npm install --save @porkchopsandwich/progress-tracker
```

## Use

### Basics

Three Progress Trackers are available:
- Sequential Progress Tracker (`const progressTracker = sequentialProgressTracker(maxStates);`)
    - Used to represent a simple progress list whose 'steps' are progressed through sequentially.
- Indexed Progress Tracker (`const progressTracker = indexedProgressTracker(maxStates);`)
    - Used to represent a progress list whose 'steps' are identified by index and may be populated out of order, or whose values may change.
- Parent Progress Tracker (`const progressTracker = parentProgressTracker();`)
    - Used to contain child progress trackers (which can also be parents). Whenever a child progresses, the parent state is updated as well.
    
In TypeScript, use a string union to limit the valid progress states for a progress tracker:
```typescript
type States = "success" | "failure";

const progressTracker = sequentialProgressTracker<States>(5);
```
    
### Progressing

Sequential Progress Trackers are progressed by passing in the next progress value, or values:
```typescript

// Single next value
progressTracker.progress("success");

// Multiple values
progressTracker.progress("success", "failure", "success");
``` 

Indexed Progress Trackers are progressed by indicating the (0-based) index to update, and the new value:
```typescript
progressTracker.progress(2, "failure");
```

### Tracking progress

Progress Trackers publish events when they are updated. These can be subscribed to with `.subscribe()`. The current state can be read at any time with `.getCurrentState()`.

```typescript
const subscription = progressTracker.subscribe((state) => {
    // Do something
});

// When finished, unsubscribe to stop receiving events
subscription.unsubscribe();

// Get the current state at any time:
const currentState = progressTracker.getCurrentState();
```

### State object

The state object received by listeners, or returned by `getCurrentState()` looks like:
- _maxState_: The maximum number of states (as defined upon creation or reset)
- _progressedStates_: The number of state progressions that have occured so far
- _stateMap_: An object whose keys are the State types ("success", "failure") and whose values are the number of times those States have been progressed.
    - Types that have not yet been progressed at all will not be present in the object.
- _stateList_: An array of the progression states as they were added.
    - For Sequential Progress Trackers this array will initially be zero-length, and will grow as entries are added.
    - For Indexed Progress Trackers this array will be pre-populated with undefined, as the progress slots can are filled by index, potentially out of order.
    
In addition, Parent Progress Trackers have:
- _childStates_: A `WeakMap` whose keys are the child progress trackers, and whose values are those trackers' current states.

For example:
```typescript 
{
    maxState: 5,
    progressedState: 3,
    stateMap: {
        success: 2,
        failure: 1,
    },
    stateList: ["success", "failure", "success"],

    // If it is a parent tracker state
    childStates: WeakMap<ProgressTracker, ProgressTrackerState>, 
}
```
